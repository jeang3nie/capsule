Meta(
    title: "Hell Week",
    summary: Some("I really hate automobiles and jobs"),
    published: Some(Time(
        year: 2022,
        month: 8,
        day: 5,
        hour: 13,
        minute: 12,
        second: 33,
    )),
    tags: [
        "automobiles",
        "work",
    ],
)
---
It was bound to happen sooner or later. But the timing really does suck.

I started hearing a clanging noise from my truck a couple of months ago, very intermittently. Something like the sound of an exhaust pipe just starting to drag on the ground. Sometimes followed by other noises, which sounded like bearing noise of some sort. I haven't been able to track it down.

Money is, of course, a critical concern right now. In our post-Covid economy, our once just adequate incomes have turned into a slow backpedal and decline. So taking it somewhere to have an expert work on it was out of the question. It's heart wrenching seeing everything that you've worked for slowly slip away.

Anyway, I came out of work a few days ago to a dead battery. Not even enough juice to turn on a dome light. My girlfriend came out and we hooked up the jumper cables, but even after ten minutes the starter still wouldn't engage. Went home, caught a ride in from her brother the following day. Brought my handy code reader with me, but yeah, not enough battery power in the truck to even read anything. Chrystal (the girlfriend) came to pick me up after work and we hooked up the jumper cables and then the code reader. No codes - apparently the engine computer needs power for it's persistent storage.

Next two days we were both off work. Went out to the truck with some tools. Pulled the battery and had it tested. The battery tester didn't even register that anything was connected to it. Thankfully, it was under warranty and they replaced it. But the starter still won't engage. So I went under the truck to check out the starter and the solenoid is so hot to the touch it burned my hand. So yeah, that starter is most likely toast. I start trying to take it out, right there in the parking lot. Of course it's both hot and lightly raining at the same time, and I'm struggling in that really confined space under a truck that hasn't even been jacked up off the ground at all. It's like a bayou in my pants when I finally give up. The top bolt is just not something I'm going to be able to reach with the tools that I brought with me.

One of my son's coworkers throws us a bone. Her husband has a car hauler and will bring the truck to the house for just some gas money. Now this is why I still love rednecks. Nice people generally, and in the country we tend to help each other out. But he can't get to it until the next day afer work. I take him up on the offer anyway, not wanting to attempt any more parking lot mechanics. But sadly, it's not going to get resolved while we're both off work.

Got the truck home at about 9PM on Wednesday. I'm already exhausted. The guy was really nice, but quite a talker. And his tow vehicle had iffy springs, so he thought he'd try to give it a break and load the truck with the engine towards the back of the trailer. Bad idea it turns out. With all that weight on the back of the trailer it pulled the hitch up high and swayed like a hula dancer the entire way to the house. But we did make it without damage to person or property.

Thursday it's back to work. Chrystal works normal day shift, but I'm on afternoons. I catch a ride from her anyway and spend the entire day stuck in town, with no way to work on the truck. It's maddening. And we can't make this work all week, because she has to go in early on Saturday. So last night I got to work at about 11:30PM. The starter comes out easily enough now that I've got all of the tools. It's in her car now, and she'll be taking it to the parts store after her shift to have it tested and get the replacement.

The good news, I'm really pretty sure that I know what happened now. The starter solenoid was getting stuck in the engaged position. The clanging was the pinion gear rattling against the flywheel. The bearing noise was from the starter spinning constantly at high speed as I drove. Which fried the starter, to the point of a dead short. Which in turn collapsed at least one cell in the battery. So, fingers crossed, a new starter will get it back on the road. At this point I'm just exhausted, frustrated, and tired of being dirty. And there's still the nagging fear that there's something else wrong.

I would give up driving in a hot second if I could. I want a remote job (anyone want to take a chance on a self taught software developer?) and to not be reliant on that damn truck being in running condition every day. I'm even more convinced that the way we've structured our society is unnatural for humans. But yeah, I'm too tired to start writing philosophy.