Meta(
    title: "Re: Gemini and the Golden Age of Air Travel",
    summary: None,
    published: Some(Time(
        year: 2022,
        month: 11,
        day: 15,
        hour: 18,
        minute: 49,
        second: 24,
    )),
    tags: [
        "gemini",
        "gatekeeping",
        "communities",
    ],
)
---
Rob S shared some thoughts that refined a bit of earlier discussion around community barriers.
=> gemini://jsreed5.org/log/2022/202211/20221115-gemini-and-the-golden-age-of-air-travel.gmi Rob s: Gemini and the Golden Age of Air Travel

Sandra over at idiomrottning has a great reply to what he wrote. I especially like this part.
> Having a small community is good.
> Having the barrier to entry be technical competence is bad.
=> <LeftMouse>gemini://idiomdrottning.org/gemini-gatekeeping Sandra's reply

This idea of having a community that is open, but only to those who are going to give thoughtful interaction is something that has been eluding human beings for a long time. Consider the Roman republic. The Roman version of democracy gave voice to the people in their governance. However, "the people" were only those members of society who were native to Rome, male, and wealthy. This was also an example of a barrier to entry which is bad. It's also basically the same mistake that the US founding fathers repeated.

Going back to Rob's post and example of the golden age of air travel. The barrier of high cost might have had the desirable results that airline passengers were well treated, and in return were polite and plleasant customers. But I tend to think that requiring wealth as a barrier to entry is actually much worse than technical competence. Technical competence is at least something that a person can realistically hope to achieve on their own effort. I'm not arguing that it is the best measure of a person's merit, because it isn't.

I see this discussion as applicable in a lot of places beyond Gemini and the early internet. I've alluded to Governemnt already. But I have had numerous discussions over the years with people who want to make Linux and open source as "easy to use" as Windows, OSX, Android or IOS.

My arguments tend to always go the same. Easy to one person and use case is not the same as easy to another person and use case. I use Arch, and sometimes FreeBSD. I find it easy to set up a system using those two operating systems the way that I want, and also easy to maintain them in running condition over time. Updates come in small batches that get applied granularly every day. My config files are not touched by the package manager. In contrast, if I try to use an "easy" Linux distribution, such as Ubuntu, the moment that I try to configure something that is off of the golden path roadblocks appear. Not only that, but once I have reconfigured things I am always worried that the next update will nuke my customizations or break them in some unforseen way. And that's still in the Linux and open source realm. If you go back to Windows, OSX, Android and IOS that situation is orders of magnitude worse. If all I wanted was to browse the web, play some games and run proprietary software those systems would be great, but because I am opinionated about what is running on my computer those systems are backbreakingly difficult for me to use in any way that I find satisfying. I use these arguments, and instead of listening to my logic those on the other side usually just say I don't want Linux to ever change and I'm part of the problem. And that I'm gatekeeping. Never mind that unlike those who usually say those things I actually contribute code to open source projects and attempt to make them better and fix bugs.

And then there's the subject I've been talking about quite a bit recently, the Twitter invasion of the fediverse. I've been at my current Mastodon instance for a year and a half. Before that I had been using Diaspora and GNUsocial for a number of years. There are plenty of people who have been there much longer than me, but I've been around long enough to have gotten comfortable with the community being relatively small, and with the general common standards and expectations that it had. I was actually kind of cringing at the prospect of a big flood of new users coming in from Twitter. I'm not convinced that it's going to be another eternal September, but there are definitely some pains.

As an example, I replied a few days ago to a post by the Raspberry PI foundation which showed off a multicolored, glowing Stripper pole which was powered by a Raspberry PI. Apparently they had shared it on Twitter because they thought it was a cool and innovative use of technology, and Twitter users did not agree, so they wanted a take from the Fediverse. My reply, which to me was a light hearted joke, was that as long as it wasn't my kid on the pole then I'm fine with it. Was it a great joke? No. I'm not especially proud of it. But I didn't really see any harm in it, either. I think most people who have young adult children hope that their kids find less exploitive methods of making a living.

What I did not expect (but probably should have) is a scathing reply questioning what went through my head and why I thought it was a good idea to post that. This young person was very, very outraged and proceeded to psychoanalize my motives and the fact that my kid having a body made me uncomfortable. It was a very Twitter like interaction. Of course when I checked where that reply came from the account was created in October of 2022. So yeah, most likely this person came from Twitter where this is one of the normal ways that you interact with people. It was, in short, one of the things that I've been fearing.

Here's the happy ending though. I did not reply. I did not lose my job over something I said on Twitter that went viral in a bad way, either. I muted that person and went about my day.

### The Point
And that is the point that I wish to impart at the end of the day. I don't really believe that we have strong barriers to entry in Gemini. If those low barriers at some point allow folks to arrive here who you dislike interacting with, you can just choose to ignore them and still continue to enjoy using Gemini. That is the strength of distributed systems. Even if someone were attempting to harass you on Gemini, it's not like you're going to get notifications popping up in your text editor about it. You can choose your level of interaction, even with zero barriers to entry.