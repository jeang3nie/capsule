Meta(
    title: "Toe 0.2 release",
    summary: Some("Toe is a Finger server with some extra security features"),
    published: Some(Time(
        year: 2022,
        month: 11,
        day: 23,
        hour: 4,
        minute: 40,
        second: 55,
    )),
    tags: [
        "finger",
        "toe",
        "rust",
    ],
)
---
Some of the recent changes to Agis had application to Toe as well, with minor changes. Toe now shuts down gracefully when receiving sigint, sigterm, etc. I've also done some general code cleanup and housekeeping.
=> https://codeberg.org/jeang3nie/Toe/releases/tag/v0.2.0

Toe is a Finger protocol server with a few extra security features such as the ability to run in a chroot and choose which system information to make available. It can be found running in the wild by visiting the `finger` link at the bottom of this page.

Binaries are available for the following platforms:
* x86_64 Linux GNU
* x86_64 Linux Musl
* x86_64 FreeBSD
* aarch64 Linux GNU