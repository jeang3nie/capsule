Meta(
    title: "Nwg shell for Sway",
    summary: Some("Nwg shell adds a lot of full blown desktop functionality back into Sway."),
    published: Some(Time(
        year: 2023,
        month: 5,
        day: 23,
        hour: 21,
        minute: 45,
        second: 48,
    )),
    tags: [
        "sway",
        "discoveries",
    ],
)
---
My previous post about migrating to Sway:
=> gemini://gemini.hitchhiker-linux.org/gemlog/migrating_to_sway.gmi Migrating to Sway

As a follow up, I found a pretty cool project called Nwg Shell which provides a lot of the functionality that you might miss from a full blown desktop environment, but for Sway specifically.
=> https://github.com/nwg-piotr/nwg-shell Nwg Shell

I'm using two of the component pieces and find that it's plenty for my uses. I've replaced Wofi with nwg-drawer as my launcher and am using nwg-panel as (surprise) my panel. Both are very attractive, very functional and look surprisingly like Gnome. It's like a best of both worlds setup.

## Nwg Drawer
=> https://github.com/nwg-piotr/nwg-drawer Repository
When open this looks a lot like the applications grid in Gnome's activity overview. It's also a nice search as you type sort of interface. The only issue I would point out is that it opens a little slower than something like rofi or wofi on slower hardware, but on my circa-2017 Dell laptop (7th gen i5) it's plenty snappy. It's written in Go using Gtk+3. Comes in at 4.7M in size, so it's a little chunky in that department, but I've seen worse these days.
=> ../images/screenshot-Tue_May_23_13:48:55_EDT_2023.png Screenshot

## Nwg Panel
=> https://github.com/nwg-piotr/nwg-panel Repository
Unlike Nwg Drawer, this one is written in Python. Again, it's using Gtk+3 though, so at least there's some consistency there. What I don't like is that I had to add some python modules that Void didn't have packages for, but if you're using Arch there's an Aur package. The default layout gives you two panels with the taksbar and weather applet on the bottom, and all of the other applets on the top. The clock is centered on the top panel and clicking on it brings up a small calendar. There is a little utility menu on the right which is a lot like Gnome's quick settings. You get a graphical settings manager with it as well, so no editing config files unless you want to. I ditched the bottom panel and moved the taksbar into the top panel because I hate taking up extra screen.
=> ../images/screenshot-Tue_May_23_13:38:40_EDT_2023.png Screenshot

## What's missing?
One thing I always loved about Plasma and Gnome is that you hit the Super key and start typing to bring up an application. I loved that enough that I set up XFCE the same way by using the Whisker menu and a small X11 app called Ksuperkey, which allows you to bind to the Super key by itself while still using it for other keycombos (otherwise your menu would still open when using Super+1 or whatever). Anyway, since Sway isn't X11 I don't have a way to duplicate that, so I've had to get used to Super+D. It's not a huge deal, but muscle memory being what it is I find myself occasionally wondering why my app launcher isn't opening.

## Gnome still pisses me off, and I'm still using Zellij
So all of what I wrote in my previous post about Gnome still applies. I'm only thinking about it because these two programs (nwg-drawer and nwg-panel) look so much like their Gnome counterparts, which I really do like. So I've basically got my best of both worlds setup now, since I have so much more room to see the content of my applications than I would in Gnome, and I've also got tiling window management.

After a few days of just opening a new terminal instance every time I wanted a terminal I did wind up switching back to using Zellij. It's just a nice way of working to me. I have the keybindings set up exactly how I want them and just find it a little more productive than only using Sway's native tiling, especially because I also have tabs available.
