This is the source code for my [gemini](https://gemini.circumlunar.space) capsule
located at [gemini.hitchhiker-linux.org](gemini://gemini.hitchhiker-linux.org).
It uses [Zond](https://codeberg.org/jeang3nie/zond) as a static capsule
generator.
