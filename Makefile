submit_url = gemini://warmedal.se/~antenna/submit?gemini%3A%2F%2Fgemini.hitchhiker-linux.org%2Fgemlog%2Fatom.xml

all: build upload publish commit

build:
	zond build

upload: build
	rsync -rP public/* nathan@hitchhiker-linux.org:/srv/gemini

tinylog:
	zond tinylog
	zond build
	scp public/tinylog.gmi nathan@hitchhiker-linux.org:/srv/gemini/
	$(MAKE) commit

publish:
	echo "$(submit_url)" | eval openssl s_client -connect warmedal.se:1965 -crlf \
		-ign_eof -quiet

commit:
	git add .
	git commit
	git push

clean:
	rm -rf capsule.tar public

.PHONY: all build clean upload publish
